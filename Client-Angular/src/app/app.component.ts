import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss',
    '../../node_modules/@elsa-workflows/elsa-workflows-studio/dist/elsa-workflows-studio/assets/fonts/inter/inter.css',
    '../../node_modules/@elsa-workflows/elsa-workflows-studio/dist/elsa-workflows-studio/elsa-workflows-studio.css'],
  encapsulation: ViewEncapsulation.ShadowDom,
})
export class AppComponent {
  title = 'AngularElsa';
}
